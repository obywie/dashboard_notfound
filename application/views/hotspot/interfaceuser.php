<script>
    function once_load() {
        getCloudUser();
    }

    function getCloudUser() {
        $('#luding').show();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url() . 'hotspot/get_cloud_user'; ?>",
            success: function (resp) {
                $('#luding').hide();
                $("#div_ip_hotspot_user").html(resp);
            },
            error: function (event, textStatus, errorThrown) {
                $('#luding').hide();
                $("#div_ip_hotspot_user").html('Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown);
            }
        });
    }

    function resetUrgentUser() {
        $('#luding').show();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url() . 'hotspot/resetUrgentUser'; ?>",
            success: function (resp) {
                $('#luding').hide();
                $("#div_ip_hotspot_user").html(resp);
            },
            error: function (event, textStatus, errorThrown) {
                $('#luding').hide();
                $("#div_ip_hotspot_user").html('Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown);
            }
        });
    }
    function resetUserId(idx) {
        $('#luding').show();
        $.ajax({
            type: "GET",
            url: "<?php echo base_url() . 'hotspot/cresetUserId/'; ?>" + idx,
            success: function (resp) {
                console.log(resp);
                $('#luding').hide();
                $("#div_ip_hotspot_user").html(resp);
            },
            error: function (event, textStatus, errorThrown) {
                $('#luding').hide();
                $("#div_ip_hotspot_user").html('Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown);
            }
        });
    }
    function newPass(idx, idelem) {

        var datax = $("#" + idelem).val();
        if (datax % 1 === 0) {
            $('#luding').show();
            $.ajax({
                type: "POST",
                data: {
                    idx: idx,
                    valuex: datax
                },
                url: "<?php echo base_url() . 'hotspot/newPass'; ?>",
                success: function (resp) {
                    console.log(resp);
                    $('#luding').hide();
                    $("#div_ip_hotspot_user").html(resp);
                },
                error: function (event, textStatus, errorThrown) {
                    $('#luding').hide();
                    $("#div_ip_hotspot_user").html('Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown);
                }
            });
        }
    }

    once_load();
</script>
<!--HEADER-->
<div class="header">
    <!--DESCRIPTION--><!-- <center><h2><span style="font-size:11px;color:#008899;">..::: PENGGUNA RTPAPAT.NET :::...</span></h2></center><!--END DESCRIPTION-->
</div>
<div class="content">
    <div class="table-responsive" id="div_ip_hotspot_user">
        <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> <i>sedang memuat data...</i>
    </div>
</div>
