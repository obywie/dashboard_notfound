<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hotspot extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('stator');
        $this->load->model('hotspot_model');
    }

    public function index() {
        $data['title'] = 'Hotspot';
        $data['listmenu'] = $this->stator->getListMenu();
        $data['sub_page'] = 'hotspot/user';
        $this->mikrostator->set('hotspotuserprofile', $this->hotspot_model->getHotspotUserProfile());
        $this->load->view('header', $data);
        $this->load->view('hotspot', $data);
        $this->load->view('footer');
    }

    public function user() {
        $data['title'] = 'Users - Hotspot';
        $data['listmenu'] = $this->stator->getListMenu();
        $data['sub_page'] = 'hotspot/user';
        $this->mikrostator->set('hotspotuserprofile', $this->hotspot_model->getHotspotUserProfile());
        $this->load->view('header', $data);
        $this->load->view('hotspot', $data);
        $this->load->view('footer');
    }

    public function interfaceuser() {
        $data['title'] = 'Interface User';
        $data['listmenu'] = $this->stator->getListMenu();
        $data['sub_page'] = 'hotspot/interfaceuser';
        $this->load->view('header', $data);
        $this->load->view('hotspot', $data);
        $this->load->view('footer');
    }

    public function vouchermeeting() {
        $data['title'] = 'Voucher Meeting';
        $data['listmenu'] = $this->stator->getListMenu();
        $data['sub_page'] = 'hotspot/vouchermeeting';
        $this->load->view('header', $data);
        $this->load->view('hotspot', $data);
        $this->load->view('footer');
    }

    public function user_public() {
        $data['title'] = 'Interface User';
        $data['listmenu'] = $this->stator->getListMenu();
        $data['sub_page'] = 'hotspot/user_public';
        $this->load->view('header', $data);
        $this->load->view('hotspot', $data);
        $this->load->view('footer');
    }

    public function user_profile() {
        $data['title'] = 'User Profile - Hotspot';
        $data['listmenu'] = $this->stator->getListMenu();
        $data['sub_page'] = 'hotspot/user_profile';
        $this->load->view('header', $data);
        $this->load->view('hotspot', $data);
        $this->load->view('footer');
    }

    public function queue_simple() {
        $data['title'] = 'Queue Simple - Hotspot';
        $data['listmenu'] = $this->stator->getListMenu();
        $data['sub_page'] = 'hotspot/queue_simple';
        $this->load->view('header', $data);
        $this->load->view('hotspot', $data);
        $this->load->view('footer');
    }

    public function restore_active() {
        $data['title'] = 'Restore Active - Hotspot';
        $data['listmenu'] = $this->stator->getListMenu();
        $data['sub_page'] = 'hotspot/restore_active';
        $this->load->view('header', $data);
        $this->load->view('hotspot', $data);
        $this->load->view('footer');
    }

    public function history() {
        $data['title'] = 'Log - Hotspot';
        $data['listmenu'] = $this->stator->getListMenu();
        $data['sub_page'] = 'hotspot/history';
        $this->load->view('header', $data);
        $this->load->view('hotspot', $data);
        $this->load->view('footer');
    }

    public function dhcp_server_leases() {
        $data['title'] = 'DHCP Server > Leases - Hotspot';
        $data['listmenu'] = $this->stator->getListMenu();
        $data['sub_page'] = 'hotspot/dhcp_server_leases';
        $this->load->view('header', $data);
        $this->load->view('hotspot', $data);
        $this->load->view('footer');
    }

    public function generate_voucher() {
        $data['title'] = 'Generate Voucher - Hotspot';
        $data['listmenu'] = $this->stator->getListMenu();
        $data['sub_page'] = 'hotspot/generate_voucher';
        $this->load->view('header', $data);
        $this->load->view('hotspot', $data);
        $this->load->view('footer');
    }

    public function sync_profile_mikrotik() {
        $result = $this->hotspot_model->syncprofilemeeting();
        redirect("hotspot/vouchermeeting");
    }

    public function get_ip_hotspot_user() {
        $result = $this->hotspot_model->getIpHotspotUser();
        return $result;
    }

    public function getControlMeetingCloud() {
        $result = $this->hotspot_model->getModelMeetingCloud();
        return $result;
    }

    public function get_cloud_user() {
        $result = $this->hotspot_model->getCloudUser();
        return $result;
    }

    public function resetUrgentUser() {
        $this->hotspot_model->resetUrgentUser();
        $result = $this->hotspot_model->getCloudUser();
        return $result;
    }

    public function cresetUserId($idx) {
        $this->hotspot_model->mresetUserId($idx);
        $result = $this->hotspot_model->getCloudUser();
        return $result;
    }

    public function cdeletemeeting($idx) {
        $this->hotspot_model->mdeletemeeting($idx);
        $result = $this->hotspot_model->getModelMeetingCloud();
        return $result;
    }

    public function get_cloud_user_public() {
        $result = $this->hotspot_model->getCloudUserPublic();
        return $result;
    }

    public function get_ip_hotspot_user_profile() {
        $result = $this->hotspot_model->getIpHotspotUserProfile();
        return $result;
    }

    public function get_queue_simple() {
        $result = $this->hotspot_model->getQueueSimple();
        return $result;
    }

    public function get_dhcp_server_leases() {
        $result = $this->hotspot_model->getDhcpServerLeases();
        return $result;
    }

    public function edit_ip_hotspot_user($username) {
        $data = $this->hotspot_model->get_ip_hotspot_user_by_username($username);
        echo json_encode($data);
    }

    function disable_ip_hotspot_user() {
        $data = array(
            'user' => $this->input->get('user'),
        );
        //$user = $data['user'];
        //echo $this->mikrostator->disableIpHotspotUser($user);
        echo $this->hotspot_model->prosesDisableIpHotspotUser($data);
    }

    function enable_ip_hotspot_user() {
        $data = array(
            'user' => $this->input->get('user'),
        );
        //$user = $data['user'];
        //echo $this->mikrostator->disableIpHotspotUser($user);
        echo $this->hotspot_model->prosesEnableIpHotspotUser($data);
    }

    function hapus_queue_simple() {
        $data = array(
            'id' => $this->input->get('id'),
            'user' => htmlentities($this->input->get('user')),
        );
        echo $this->hotspot_model->prosesHapusQueueSimple($data);
    }

    function simpan_tambah_hotspot_user() {
        $data = array(
            'user' => $this->input->post('user'),
            'password' => $this->input->post('password'),
            'userprofile' => $this->input->post('userprofile'),
            'server' => 'all',
        );
        echo $this->hotspot_model->prosesTambahHotspotUser($data);
    }

    function simpan_update_hotspot_user() {
        $data = array(
            'user' => $this->input->post('user'),
            'password' => $this->input->post('password'),
            'userprofile' => $this->input->post('userprofile'),
        );
        echo $this->hotspot_model->prosesUpdateHotspotUser($data);
        //echo $this->mikrostator->editHotspotUser($data);
        //echo print_r($data);
    }

    function simpan_tambah_hotspot_meeting() {
        $data = array(
            'active_date' => $this->input->post('active_date'),
            'expired_date' => $this->input->post('expired_date'),
            'meetingroom' => $this->input->post('meetingroom'),
            'meetingname' => $this->input->post('meetingname'),
            'userprofile' => $this->input->post('userprofile')
        );
        echo $this->hotspot_model->prosesTambahHotspotMeeting($data);
    }

    function hapus_ip_hotspot_user() {
        $data = array(
            'id' => $this->input->get('id'),
            'user' => $this->input->get('user'),
        );
        echo $this->hotspot_model->prosesHapusIpHotspotUser($data);
    }

    public function get_restore_active() {
        $result = $this->hotspot_model->getRestoreActive();
        return $result;
    }

    public function get_restore_table_jam($jam) {
        return $this->hotspot_model->getRestoreTableJam($jam);
    }

    public function proses_restore_table_jam($jam) {
        return $this->hotspot_model->prosesRestoreTableJam($jam);
    }

    public function get_history_mikrotik() {
        $result = $this->hotspot_model->getHistoryMikrotik();
        return $result;
    }

    public function get_history_jam() {
        $result = $this->hotspot_model->getHistoryJam();
        return $result;
    }

}

/* End of file hotspot.php */
/* Location: ./application/controllers/hotspot.php */